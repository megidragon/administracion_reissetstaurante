<?php

use yii\db\Migration;

/**
 * Class m190122_043602_add_deleted_food_and_plate
 */
class m190122_043602_add_deleted_food_and_plate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('foods', 'deleted', $this->boolean()->defaultValue(0));
        $this->addColumn('plates', 'deleted', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('foods', 'deleted');
        $this->dropColumn('plates', 'deleted');
    }
}
