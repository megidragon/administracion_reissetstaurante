<?php

use yii\db\Migration;

/**
 * Class m181128_142918_create_all_tables
 */
class m181128_142918_create_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("coupons", [
            'id' => $this->primaryKey(),
            'code' => $this->string(),
            'type' => "ENUM('percentage', 'number', 'free') DEFAULT 'number'",
            'amount' => $this->integer(),
            'expiration_time' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("foods", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'plate_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'cost' => $this->double(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("ingredients", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'cost' => $this->double(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("menu_items", [
            'menu_id' => $this->integer(),
            'plate_id' => $this->integer(),
        ]);

        $this->createTable("menus", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("movements", [
            'id' => $this->primaryKey(),
            'cost' => $this->double(),
            'origin' => $this->string(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("offers", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'amount' => $this->integer(),
            'type' => "ENUM('percentage', 'number') DEFAULT 'number'",
            'remains' => $this->integer(),
            'cumulative' => $this->boolean(),
            'days' => $this->string(),
            'active' => $this->boolean(),
            'establishment_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("order_plates", [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'plate_id' => $this->integer(),
            'food_id' => $this->integer(),
            'amount' => $this->integer(),
            'description' => $this->text(),
            'extra_cost' => $this->double(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("order_type", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'key' => $this->string()
        ]);

        $this->createTable("orders", [
            'id' => $this->primaryKey(),
            'additional' => $this->double(),
            'movement_id' => $this->integer(),
            'order_type_id' => $this->integer(),
            'table_id' => $this->integer(),
            'coupon_id' => $this->integer(),
            'offer_id' => $this->integer(),
            'observations' => $this->text(),
            'completed' => $this->boolean(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'establishment_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("password_resets", [
            'email' => $this->string(),
            'token' => $this->string(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("plates", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'img_url' => $this->string()->defaultValue('img/default.png'),
            'cost' => $this->double(),
            'amount' => $this->integer(),
            'establishment_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("preparation", [
            'food_id' => $this->integer(),
            'ingredient_id' => $this->integer()
        ]);

        $this->createTable("summary", [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp(),
        ]);

        $this->createTable("tables", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'establishment_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("users", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'password' => $this->string(),
            'privileges' => $this->integer(),
            'remember_token' => $this->string(),
            'establishment_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable("establishments", [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $type = new \app\models\OrderType();
        $type->name = 'local';
        $type->key = 'local';
        $type->insert();

        $type = new \app\models\OrderType();
        $type->name = 'Retiro';
        $type->key = 'retiro';
        $type->insert();

        $table = new \app\models\Establishments();
        $table->id = 1;
        $table->name = 'La Fonda';
        $table->insert();

        $user = new \app\models\Users();
        $user->id = 1;
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = sha1('abc789');
        $user->privileges = 1;
        $user->establishment_id = 1;
        $user->insert();


        $table = new \app\models\Establishments();
        $table->id = 2;
        $table->name = 'Local plaza';
        $table->insert();

        $table = new \app\models\Establishments();
        $table->id = 3;
        $table->name = 'Local guillon';
        $table->insert();

        $table = new \app\models\Establishments();
        $table->id = 4;
        $table->name = 'Pizza 10';
        $table->insert();

        $table = new \app\models\Tables();
        $table->name = 'Sin mesa';
        $table->establishment_id = 1;
        $table->insert();

        $table = new \app\models\Tables();
        $table->name = 'Sin mesa';
        $table->establishment_id = 2;
        $table->insert();

        $table = new \app\models\Tables();
        $table->name = 'Sin mesa';
        $table->establishment_id = 3;
        $table->insert();

        $table = new \app\models\Tables();
        $table->name = 'Sin mesa';
        $table->establishment_id = 4;
        $table->insert();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_142918_create_all_tables cannot be reverted.\n";

        return false;
    }
}
