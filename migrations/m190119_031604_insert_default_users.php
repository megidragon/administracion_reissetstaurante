<?php

use yii\db\Migration;

/**
 * Class m190119_031604_insert_default_users
 */
class m190119_031604_insert_default_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new \app\models\Users();
        $user->id = 2;
        $user->name = 'usuario1';
        $user->email = 'admin@admin.com';
        $user->password = sha1('12345');
        $user->privileges = 2;
        $user->establishment_id = 2;
        $user->insert();

        $user = new \app\models\Users();
        $user->id = 3;
        $user->name = 'usuario2';
        $user->email = 'admin@admin.com';
        $user->password = sha1('12345');
        $user->privileges = 2;
        $user->establishment_id = 3;
        $user->insert();

        $user = new \app\models\Users();
        $user->id = 4;
        $user->name = 'usuario3';
        $user->email = 'admin@admin.com';
        $user->password = sha1('12345');
        $user->privileges = 2;
        $user->establishment_id = 4;
        $user->insert();

        $user = new \app\models\Users();
        $user->id = 5;
        $user->name = 'usuario4';
        $user->email = 'admin@admin.com';
        $user->password = sha1('12345');
        $user->privileges = 2;
        $user->establishment_id = 1;
        $user->insert();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190119_031604_insert_default_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190119_031604_insert_default_users cannot be reverted.\n";

        return false;
    }
    */
}
