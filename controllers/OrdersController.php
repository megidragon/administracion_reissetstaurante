<?php

namespace app\controllers;

use app\components\Calculate;
use app\models\Movements;
use app\models\Offers;
use app\models\OrderPlates;
use app\models\OrderPlatesForm;
use app\models\OrderType;
use app\models\Plates;
use DateTime;
use app\models\Tables;
use Yii;
use yii\web\Controller;
use app\models\Orders;
use app\models\OrdersForm;
use yii\helpers\Html;
use yii\helpers\Url;


class OrdersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'data' || $action->id == 'calculate-totals') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionList()
    {
        return $this->render('list');
    }

    public function actionData(){
        $data = Yii::$app->request->post();
        $pos = ['tables.name', 'orders.created_at', 'movements.cost', 'orders.observations', 'orders.completed', 'orders.id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        // TODO: Añadir mas opciones de busqueda
        $conditions = ($searchName) ? "description LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $query = Orders::find()
            ->joinWith(['tables', 'movements', 'orderType'])
            ->where($conditions);
        if (Yii::$app->user->identity->id !== 1)
        {
            $query = $query->andWhere(['orders.establishment_id' => Yii::$app->user->identity->establishment_id]);
        }

        if ($data['StatusFilter'] || $data['StatusFilter'] === '0')
        {
            $query = $query->andWhere(['orders.completed' => $data['StatusFilter']]);
        }

        if ($data['DateMinFilter']) {
            $data['DateMinFilter'] = DateTime::createFromFormat('d / m / Y', $data['DateMinFilter'])->format('Y-m-d');
            $query->andWhere("DATE_FORMAT(orders.created_at, '%Y-%m-%d') >= DATE_FORMAT('{$data['DateMinFilter']}', '%Y-%m-%d')");
        }

        if ($data['DateMaxFilter']) {
            $data['DateMaxFilter'] = DateTime::createFromFormat('d / m / Y', $data['DateMaxFilter'])->format('Y-m-d');
            $query->andWhere("DATE_FORMAT(orders.created_at, '%Y-%m-%d') <= DATE_FORMAT('{$data['DateMaxFilter']}', '%Y-%m-%d')");
        }



        $result = $query->orderBy([$order => $orderDir])
            ->limit($data['length'])
            ->offset($data['start'])
            ->all();
        $total = Orders::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $buttons = '';
            if ($row->completed == 0) {
                $completed = 'En preparacion';
                $buttons .= Html::button('<i class="fa fa-check"></i>', ['class'=>"btn btn-sm btn-info", 'onclick' => "processButton({$row->id})", 'data-toggle'=>"tooltip", 'data-id' => $row->id, 'title'=>"Procesar"]);
                $buttons .= Html::a(Html::button('<i class="fa fa-cog"></i>', ['class'=>"btn btn-sm btn-primary", 'data-toggle'=>"tooltip", 'title'=>"Modificar"]), ['/orders/modify', 'id' => $row->id]);
                if (Yii::$app->user->identity->id === 1) {
                    $buttons .= Html::a(Html::button('<i class="fa fa-trash"></i>', ['class' => "btn btn-sm btn-danger", 'data-toggle' => "tooltip", 'title' => "Eliminar"]), ['/orders/delete', 'id' => $row->id]);
                }else{
                    $buttons .= Html::a(Html::button('<i class="fa fa-trash"></i>', ['class' => "btn btn-sm btn-danger", 'data-toggle' => "tooltip", 'title' => "Eliminar"]), ['/orders/process-order', 'id' => $row->id, 'completed' => 0]);
                }
            }
            else
            {
                $completed = $row->completed > 0 ? 'Entregado' : 'Cancelado';
            }


            $response['data'][] = [
                $row->tables->name,
                Yii::$app->formatter->format($row->created_at, 'datetime'),
                Yii::$app->formatter->format($row->movements->cost, 'currency'),
                $row->observations,
                $completed,
                $buttons
            ];
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    public function actionNew()
    {
        $orderModel = new OrdersForm();
        $platesModel = new OrderPlatesForm;

        if (Yii::$app->request->isPost)
        {

            $post_data = Yii::$app->request->post();
            $plates = $post_data['plates'];

            // Elimina los platos mal cargados o con formato corrupto
            $plates = $platesModel->customFilter($plates);
            $orderModel->load($post_data);

            // Valida si esta bien, sino explota.
            if (!$orderModel->validate())
            {
                throw new \Exception('Parametros no son validos: '.implode($orderModel, ', '));
            }

            if (empty($plates)) throw new \Exception("No se ingresaron los platos correctamente.");

            $transaction = Yii::$app->db->beginTransaction();

            try
            {
                $totals = Calculate::calculateOrder($plates, $orderModel);
                // Ingresa el movimiento en la base de datos
                $movement = new Movements();
                $movement->cost = $totals['total'];
                $movement->origin = 'order';
                $movement->user_id = Yii::$app->user->identity->id;
                $movement->insert();

                $movement_id = Yii::$app->db->getLastInsertID();

                // Crea la entidad principal de la orden
                $db_models = new Orders();
                $db_models->additional = $orderModel->additional;
                $db_models->movement_id = $movement_id;
                $db_models->order_type_id = $orderModel->order_type_id;
                $db_models->table_id = $orderModel->table_id;
                $db_models->coupon_id = $orderModel->coupon;
                $db_models->offer_id = $orderModel->offer_id;
                $db_models->observations = $orderModel->observations;
                $db_models->completed = 0;
                $db_models->establishment_id = Yii::$app->user->identity->establishment_id;
                $db_models->user_id = Yii::$app->user->identity->id;
                $db_models->created_at = date('Y-m-d H:i:s');
                $db_models->insert();

                $order_id = Yii::$app->db->getLastInsertID();

                foreach ($plates as $plate)
                {
                    $order_plate = new OrderPlates();
                    $order_plate->order_id      = $order_id;
                    $order_plate->plate_id      = $plate['plate_id'];
                    $order_plate->food_id       = $plate['food_id'];
                    $order_plate->amount        = $plate['amount'];
                    $order_plate->description   = $plate['description'];
                    $order_plate->extra_cost    = $plate['extra_cost'];
                    $order_plate->insert();
                }

                $transaction->commit();

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1, 'redirect' => Url::to(['orders/list']), 'msg' => 'Orden creada con exito, redirigiendo al listado.'];
                }
                return $this->redirect(['/orders/list']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        $order_types = OrderType::find()->all();
        if (Yii::$app->user->identity->id !== 1) {
            $tables = Tables::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->all();
            $offers = Offers::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->all();
            $plateList = Plates::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->andWhere(['plates.deleted' => 0])->all();
        }else{
            $tables = Tables::find()->all();
            $offers = Offers::find()->all();
            $plateList = Plates::find()->where(['plates.deleted' => 0])->all();
        }

        $orderModel->order_type_id = 1;
        $platesModel->amount = 1;

        return $this->render('new', [
            'model' => $orderModel,
            'platesModel' => $platesModel,
            'order_types' => $order_types,
            'tables' => $tables,
            'offers' => $offers,
            'plateList' => $plateList,
        ]);
    }

    public function actionModify($id){
        $orderModel = new OrdersForm();
        $platesModel = new OrderPlatesForm;

        $order = Orders::find()->joinWith(['tables', 'plates', 'foods', 'movements', 'orderType'])->where(['orders.id' => $id])->one();
        if (empty($order))
        {
            throw new \Exception("Orden no encontrada.");
        }

        if (Yii::$app->request->isPost)
        {

            $post_data = Yii::$app->request->post();
            $plates = $post_data['plates'];

            // Elimina los platos mal cargados o con formato corrupto
            $plates = $platesModel->customFilter($plates);
            $orderModel->load($post_data);

            // Valida si esta bien, sino explota.
            if (!$orderModel->validate())
            {
                throw new \Exception('Parametros no son validos: '.implode($orderModel, ', '));
            }

            if (empty($plates)) throw new \Exception("No se ingresaron los platos correctamente.");

            $transaction = Yii::$app->db->beginTransaction();

            try
            {
                $totals = Calculate::calculateOrder($plates, $orderModel);
                // Ingresa el movimiento en la base de datos
                $movement = Movements::find()->where(['id' => $order->movement_id])->one();
                $movement->cost = $totals['total'];
                $movement->origin = 'order';
                $movement->user_id = Yii::$app->user->identity->id;
                $movement->updated_at = date('Y-m-d H:i:s');
                $movement->save();

                // Crea la entidad principal de la orden
                $order->additional = $orderModel->additional;
                $order->order_type_id = $orderModel->order_type_id;
                $order->table_id = $orderModel->table_id;
                $order->coupon_id = $orderModel->coupon;
                $order->offer_id = $orderModel->offer_id;
                $order->observations = $orderModel->observations;
                $order->completed = 0;
                $order->establishment_id = Yii::$app->user->identity->establishment_id;
                $order->updated_at = date('Y-m-d H:i:s');
                $order->save();

                // Elimina los viejos platos añadidos para dar paso a los nuevos.
                OrderPlates::deleteAll(['order_id' => $id]);

                foreach ($plates as $plate)
                {
                    $order_plate = new OrderPlates();
                    $order_plate->order_id      = $order->id;
                    $order_plate->plate_id      = $plate['plate_id'];
                    $order_plate->food_id       = $plate['food_id'];
                    $order_plate->amount        = $plate['amount'];
                    $order_plate->description   = $plate['description'];
                    $order_plate->extra_cost    = $plate['extra_cost'];
                    $order_plate->insert();
                }

                $transaction->commit();

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1, 'redirect' => Url::to(['orders/list']), 'msg' => 'Orden modificada con exito, redirigiendo al listado.'];
                }
                return $this->redirect(['/orders/list']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        $order_types = OrderType::find()->all();

        if (Yii::$app->user->identity->id !== 1) {
            $tables = Tables::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->all();
            $offers = Offers::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->all();
            $plateList = Plates::find()->where(['establishment_id' => Yii::$app->user->identity->establishment_id])->andWhere(['plates.deleted' => 0])->all();
        }else{
            $tables = Tables::find()->all();
            $offers = Offers::find()->all();
            $plateList = Plates::find()->where(['plates.deleted' => 0])->all();
        }

        $orderModel->table_id = $order->table_id;
        $orderModel->coupon = /*$order->table_id*/ null;
        $orderModel->offer_id = /*$order->table_id*/ null;
        $orderModel->observations = $order->observations;
        $orderModel->additional = $order->additional;

        $orderModel->order_type_id = 1;
        $platesModel->amount = 1;

        $orderPlates = [];
        foreach ($order->orderPlates as $i => $orderPlate)
        {
            $orderPlates[] = [
                $orderPlate->plates->name,
                ($orderPlate->foods) ? $orderPlate->foods->name : 'Ninguno',
                $orderPlate['plate_id'],
                $orderPlate['food_id'],
                $orderPlate['amount'],
                $orderPlate['description'],
                $orderPlate['extra_cost'],
            ];
        }

        return $this->render('modify', [
            'model' => $orderModel,
            'platesModel' => $platesModel,
            'order_types' => $order_types,
            'tables' => $tables,
            'offers' => $offers,
            'plateList' => $plateList,
            'orderPlates' => $orderPlates,
        ]);
    }

    public function actionDelete($id){
        $model = Orders::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("Plate not found");
        }
        $model->delete();
        return $this->redirect(['orders/list']);
    }

    public function actionCalculateTotals()
    {
        $post_data = Yii::$app->request->post();
        $plates = [];
        foreach ($post_data['data'] as $plate)
        {
            $plates[] = [
                'plate_id' => $plate[2],
                'food_id' => $plate[3],
                'amount' => $plate[4],
                'description' => $plate[5],
                'extra_cost' => $plate[6],
            ];
        }

        $form_data = [
            'additional' => $post_data['form_data'][3]['value'],
        ];

        $total = Calculate::calculateOrder($plates, $form_data);

        $response = [
            'total' => $total['total'],
            'discount' => 0,
            'plates' => [],
        ];

        foreach ($total['plates'] as $i => $plate)
        {
            $response['plates'][] = [
                $post_data['data'][$i][0],
                $post_data['data'][$i][1],
                $plate['plate_id'],
                $plate['food_id'],
                $plate['amount'],
                $plate['description'],
                $plate['extra_cost'],
                $plate['total'],
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    public function actionProcessOrder($id, $completed)
    {
        $order = Orders::find()->where(['id' => $id])->one();
        if (empty($order)) {
            throw new \Exception('No se encuentra la orden');
        }

        $order->completed = $completed ? 1 : -1;
        $order->save();
        return $this->redirect(['orders/list']);
    }
}