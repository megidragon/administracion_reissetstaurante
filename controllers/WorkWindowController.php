<?php

namespace app\controllers;

use app\models\Orders;
use Yii;
use yii\web\Controller;
use app\models\Plates;
use yii\helpers\Html;

class WorkWindowController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['data', 'get-additionals'])) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionList()
    {
        return $this->render('list');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = Orders::find()
            ->joinWith(['tables', 'plates', 'foods', 'movements', 'orderType'])
            ->where(['orders.completed' => 0, 'DATE_FORMAT(orders.created_at, "%Y-%m-%d")' => date('Y-m-d')])
            // TODO: Cambiar por roles y establecimientos
            ->andWhere(['orders.establishment_id' => Yii::$app->user->identity->establishment_id])
            ->orderBy(['orders.id' => SORT_DESC])
            ->limit(16)
            ->all();

        $response = [];
        foreach ($result as $order)
        {
            $plates = [];
            foreach ($order->orderPlates as $orderPlate)
            {
                $platename = ($orderPlate->plates) ? $orderPlate->plates->name : null;
                $foodname = ($orderPlate->foods) ? $orderPlate->foods->name : '';

                $plates[] = [
                    'plate' => $platename,
                    'food' => $foodname,
                    'description' => $orderPlate->description,
                    'amount' => $orderPlate->amount
                ];
            }

            $response[] = [
                'table' => $order->tables->name,
                'plates' => $plates
            ];
        }

        return $response;
    }

    public function actionGetPlate($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (empty($id)){
            return ['data' => []];
        }
        $plate = Plates::find()->joinWith('foods')->where(['plates.id' => $id])->one();

        return ['data' => $plate, 'foods' => $plate->foods];
    }
}