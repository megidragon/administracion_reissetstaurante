<?php

namespace app\controllers;

use app\models\Establishments;
use app\models\Orders;
use Yii;
use yii\web\Controller;
use app\models\Tables;
use yii\helpers\Html;
use yii\web\UploadedFile;


class TablesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['data', 'get-additionals'])) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionList()
    {
        return $this->render('list');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['id', 'name', 'description', 'cost', 'created_at', 'id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "name LIKE '%$searchName%' OR description LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = Tables::find()->joinWith('establishment')->where($conditions)->orderBy([$order => $orderDir])->limit($data['length'])->offset($data['start'])->all();
        $total = Tables::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                Yii::$app->formatter->format($row->created_at, 'datetime'),
                $row->name,
                $row->establishment->name,
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['tables/modify', 'id' => $row->id]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i></button>', ['tables/delete', 'id' => $row->id])
            ];
        }

        return $response;
    }

    public function actionNew()
    {
        $model = new Tables();

        if (Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if (!$model->validate())
            {
                throw new \Exception($model->errors);
            }

            // If insert successfull redirect to list...
            try
            {
                $model->insert();

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['tables/list']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('new', ['model' => $model, 'establishments' => Establishments::find()->all()]);
    }

    public function actionModify($id){
        $model = Tables::find()->where(['Id' => $id])->one();

        if (Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if (!$model->validate())
            {
                throw new \Exception($model->errors);
            }

            // If insert successfull redirect to list...
            try
            {
                $model->save();

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['tables/list']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('modify', ['model' => $model, 'establishments' => Establishments::find()->all()]);
    }

    public function actionDelete($id){
        $transaction = Yii::$app->db->beginTransaction();
        $model = Tables::find()->where(['id' => $id])->one();
        try
        {
            if ($model && Orders::find()->where(['table_id' => $id])->count() <= 0)
            {
                throw new \Exception('No se permite eliminar una mesa si tiene ordenes existentes.');
            }
            $model->delete();
            $transaction->commit();
            return $this->redirect(['tables/list']);
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            throw new \Exception($e);
        }
    }
}