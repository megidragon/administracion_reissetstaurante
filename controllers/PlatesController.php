<?php

namespace app\controllers;

use app\components\Notificacion;
use app\models\Establishments;
use app\models\Foods;
use app\models\FoodsForm;
use app\models\OrderPlates;
use app\models\Orders;
use Yii;
use yii\web\Controller;
use app\models\Plates;
use app\models\PlatesForm;
use yii\helpers\Html;
use yii\web\UploadedFile;


class PlatesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['data', 'get-additionals'])) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionList()
    {
        return $this->render('list');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['id', 'name', 'description', 'cost', 'created_at', 'id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "name LIKE '%$searchName%' OR description LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $query = Plates::find()
                        ->where($conditions)
                        ->andWhere(['plates.deleted' => 0]);

        if (Yii::$app->user->identity->id !== 1)
        {
            $query = $query->andWhere(['establishment_id' => Yii::$app->user->identity->establishment_id]);
        }

        $result = $query->orderBy([$order => $orderDir])
                        ->limit($data['length'])
                        ->offset($data['start'])
                        ->all();

        $total = Plates::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                Html::img($row->img_url, ['class' => 'pull-left img-responsive image-small']),
                Yii::$app->formatter->format($row->created_at, 'datetime'),
                $row->name,
                $row->description,
                Yii::$app->formatter->format($row->cost, 'currency'),
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['/plates/modify', 'id' => $row->id]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i></button>', ['/plates/delete', 'id' => $row->id])
            ];
        }

        return $response;
    }

    public function actionNew()
    {
        $platesForm = new PlatesForm();
        $foodsModel = [new Foods];

        if (Yii::$app->request->isPost)
        {
            $platesForm->load(Yii::$app->request->post());
            if (!$platesForm->validate())
            {
                throw new \Exception($platesForm->errors);
            }
            $foods = FoodsForm::customFilter(Yii::$app->request->post()['Foods']);

            $transaction = Yii::$app->db->beginTransaction();
            // Sube la foto del plato primero.
            $platesForm->img_url = UploadedFile::getInstance($platesForm, 'img_url');

            $db_models = new Plates();
            $db_models->name        = $platesForm->name;
            $db_models->description = $platesForm->description;
            $db_models->cost        = $platesForm->cost;
            $db_models->establishment_id = Yii::$app->user->identity->establishment_id;
            $db_models->insert();
            $plate_id = Yii::$app->db->getLastInsertID();

            // Insertacada uno de las comidas adicionales del plato y los relaciona con el id del plato ingresado.
            if (!empty($foods))
            {
                foreach ($foods as $food)
                {
                    $new_food = new Foods;
                    $new_food->plate_id = $plate_id;
                    $new_food->name = $food['name'];
                    $new_food->description = $food['description'];
                    $new_food->cost = $food['cost'];
                    $new_food->insert();
                }
            }

            $image = $platesForm->upload(null, $plate_id);
            if ($image)
            {
                $db_models->img_url = $image;
            }

            // If insert successfull redirect to list...
            try
            {
                $db_models->save();

                $transaction->commit();

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['/plates/list']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('new', ['model' => $platesForm, 'foodsModel' => (empty($foodsModel)) ? [new Foods] : $foodsModel]);
    }

    public function actionModify($id){
        $platesForm = new PlatesForm();
        $foodsModel = Foods::find()->where(['plate_id' => $id, 'foods.deleted' => false])->all();
        $db_models = Plates::find()->where(['id' => $id, 'deleted' => false])->one();

        if (Yii::$app->request->isPost)
        {
            $platesForm->load(Yii::$app->request->post());
            if (!$platesForm->validate())
            {
                throw new \Exception($platesForm->errors);
            }
            $foods = FoodsForm::customFilter(Yii::$app->request->post()['Foods']);

            $transaction = Yii::$app->db->beginTransaction();

            $db_models->name        = $platesForm->name;
            $db_models->description = $platesForm->description;
            $db_models->cost        = $platesForm->cost;
            $db_models->establishment_id = Yii::$app->user->identity->establishment_id;
            $db_models->updated_at  = date('Y-m-d H:i:s');

            // Insertacada uno de las comidas adicionales del plato y los relaciona con el id del plato ingresado.
            Foods::softDeleteAll(['plate_id' => $db_models->id]);
            foreach ($foods as $food)
            {
                $new_food = new Foods;
                $new_food->plate_id = $db_models->id;
                $new_food->name = $food['name'];
                $new_food->description = $food['description'];
                $new_food->cost = empty($food['cost']) ? 0 : $food['cost'];
                $new_food->insert();
            }

            $platesForm->img_url = UploadedFile::getInstance($platesForm, 'img_url');
            $image = $platesForm->upload($db_models->img_url, $db_models->id);

            if ($image)
            {
                $db_models->img_url = $image;
            }


            // If save successfull redirect to list...
            try
            {
                $db_models->save();
                $transaction->commit();
                return $this->redirect(['plates/list']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en la institucion: ".$e->getMessage());
            }
        }

        // Set value to inputs
        $platesForm->name        = $db_models->name;
        $platesForm->description = $db_models->description;
        $platesForm->img_url     = $db_models->img_url;
        $platesForm->cost        = $db_models->cost;

        return $this->render('modify', ['model' => $platesForm, 'foodsModel' => (empty($foodsModel)) ? [new Foods] : $foodsModel]);
    }

    public function actionDelete($id){
        $transaction = Yii::$app->db->beginTransaction();

        try
        {
            if (OrderPlates::find()->where(['plate_id' => $id])->count())
            {
                throw new \Exception("No se permite eliminar platos con ordenes existentes.");
            }

            Foods::softDeleteAll(['plate_id' => $id]);
            $model = Plates::find()->where(['Id' => $id])->one();

            if (empty($model)) {
                throw new \Exception("Plate not found");
            }

            $model->deleted = true;
            $model->save();

            $transaction->commit();
            return $this->redirect(['plates/list']);
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            throw new \Exception($e);
        }
    }

    public function actionGetPlate($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (empty($id)){
            return ['data' => []];
        }
        $plate = Plates::find()->joinWith('foods')->where(['plates.id' => $id])->one();

        return ['data' => $plate, 'foods' => $plate->foods];
    }
}