<?php
namespace app\components;

use app\models\OrdersForm;
use app\models\Plates;
use app\models\Foods;

class Calculate
{
    const IVA = 0.21; // Valor absoluto iva 21%

    public static function calculateOrder($plates, $form_data)
    {
        $total_plates = 0;
        $response = [
            'plates' => []
        ];

        // Trae precio de cada plato
        foreach ($plates as $plate)
        {
            $plate_db = Plates::find()->where(['id' => $plate['plate_id']])->one();
            $food_db = (!empty($plate['food_id'])) ? Foods::find()->where(['id' => $plate['food_id']])->one()->cost : 0;

            $plate_cost = (floatval($plate_db->cost) + floatval($food_db) + floatval($plate['extra_cost'])) * floatval($plate['amount']);
            $total_plates += $plate_cost;
            $response['plates'][] = array_merge($plate, ['total' => $plate_cost]);
        }

        // Ofertas
        // TODO: No implementado aun

        // Cupones
        // TODO: No implementado aun

        // Valor adicional
        $additional = $form_data['additional'] ? floatval($form_data['additional']) : 0;
        $cost = floatval($total_plates) + $additional;

        $response['total'] = $cost;

        return $response;
    }
}