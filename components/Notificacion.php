<?php
namespace app\components;
use app\models\ConfiguracionesGenerales;
use app\models\Registro;
use yii\helpers\Url;
use Yii;

class Notificacion
{
    public static function Notificar($accion, $detalles, $referido = null, $enviar = false, $to = null)
    {
        try {
            if (is_array($referido)) {
                $referido = Url::to($referido);
            }

            $registro = new Registro();
            $registro->Accion = $accion;
            $registro->Detalles = $detalles;
            $registro->Referido = $referido;
            $registro->IdUsuario = Yii::$app->user->identity->Id;
            $registro->CreatedAt = date('Y-m-d H:i:s');

            $registro->save();
            if ($enviar) {
                $config = ConfiguracionesGenerales::find()->one();
                if (!$config->EmailServidor || !$config->EmailServidor || !$config->EmailUsuario || !$config->EmailPassword || !$config->EmailPort) {
                    return false;
                }

                $to = is_array($to) ? array_filter($to) : $to;
                if (!$to) {
                    $to = Yii::$app->user->identity->getConfiguration()->EmailRespuesta;
                    if (!$to) {
                        Yii::error("No se pudo definir email de respuesta.");
                    }
                }
                // aca enviaria el email de notificacion

                $transport = [
                    'class' => 'Swift_SmtpTransport',
                    'host' => $config->EmailServidor,
                    'username' => $config->EmailUsuario,
                    'password' => $config->EmailPassword,
                    'port' => (string)$config->EmailPort,
                    'encryption' => ($config->EmailSSL) ? 'ssl' : 'tls',
                ];

                Yii::$app->mailer->setTransport($transport);
                $send = Yii::$app->mailer->compose('template', ['asunto' => $accion, 'message' => $detalles])
                    ->setFrom('precotizador@proram.com')
                    ->setSubject('Sistema de precotizacion proram: ' . $accion);

                if (!empty($to))
                {
                    $send = $senf->setTo($to);
                }

                if ($config->EmailRespuesta)
                {
                    $send = $send->setBcc($config->EmailRespuesta);
                }

                $send->send();
                return $send;
            }

            return true;
        }catch (\Exception $e){
            Yii::error($e->getMessage());
            return false;
        }
    }
}