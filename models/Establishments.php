<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Establishments extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'establishments';
    }

    public function getId()
    {
        return $this->id;
    }
}