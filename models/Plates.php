<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Plates extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'plates';
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFoods()
    {
        return $this->hasMany(Foods::className(), ['plate_id' => 'id'])->andOnCondition(['foods.deleted' => false])->orderBy(['name' => SORT_DESC]);
    }

}