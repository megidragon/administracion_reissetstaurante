<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class OrderType extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'order_type';
    }

    public function getId()
    {
        return $this->id;
    }
}