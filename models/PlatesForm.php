<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PlatesForm extends Model
{
    public $id;
    public $name;
    public $description;
    public $cost;
    public $img_url;
    public $created_at;
    public $updated_at;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'description', 'cost',], 'required', 'message' => 'Este campo es requerido.'],
            [['name', 'description'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
            [['cost'], 'double', 'max' => 1000000, 'tooBig' => 'El sistema no soporta numeros tan grandes de {attribute}.', 'message' => 'Numero debe ser valido'],
            [['img_url'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }


    public function upload($old_pic=null, $id)
    {
        if ($this->validate()) {
            $image = false;
            if (!empty($this->img_url))
            {
                if ($old_pic)
                {
                    unlink(str_replace('@web/', '', $old_pic));
                }

                $image = "img/plates/$id.{$this->img_url->extension}";
                $this->img_url->saveAs($image);
                $image = "@web/$image";
            }


            return $image;
        } else {
            return false;
        }
    }

    public static function customFilter($array)
    {
        if (empty($array)) return false;

        foreach ($array as $i => $element)
        {
            if (empty($element['id']) || empty($element['amount']) || (!empty($element['cost']) && !is_numeric($element['cost'])) || strlen($element['description']) > 255){
                unset($array[$i]);
            }
        }

        return $array;
    }
}
