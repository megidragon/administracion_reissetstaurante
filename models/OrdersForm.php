<?php

namespace app\models;

use Yii;
use yii\base\Model;

class OrdersForm extends Model
{
    public $id;
    public $order;
    public $observations;
    public $table_id;
    public $order_type_id;
    public $additional;
    public $cost;
    public $coupon;
    public $offer_id;
    public $created_at;
    public $updated_at;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [[
                'table_id',
                'order_type_id',
            ], 'required', 'message' => 'Este campo es requerido.'],

            [[
                'order',
                'observations',
                'table_id',
                'order_type_id',
            ], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],

            [[
                'cost',
                'additional',
            ], 'double', 'max' => 1000000, 'tooBig' => 'El sistema no soporta numeros tan grandes de {attribute}.', 'message' => 'Numero debe ser valido'],
        ];
    }
}
