<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Tables extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'tables';
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'establishment_id'], 'required', 'message' => 'Este campo es requerido.'],
            [['name'], 'string', 'max' => 255, 'tooLong' => 'Se exedio del numero maximo de caracteres.']
        ];
    }

    public function getEstablishment()
    {
        return $this->hasOne(Establishments::className(), ['id' => 'establishment_id']);
    }

    public function thisEstablishment(){
        $this->andWhere(['establishment_id' => Yii::$app->user->identity->establishment_id]);
        return $this;
    }
}