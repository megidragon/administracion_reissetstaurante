<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class OrderPlates extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'order_plates';
    }

    public function getId()
    {
        return $this->id;
    }

    function getPlates()
    {
        return $this->hasOne(Plates::className(), ['id' => 'plate_id']);
    }

    function getFoods()
    {
        return $this->hasOne(Foods::className(), ['id' => 'food_id']);
    }
}