<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Offers extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'offers';
    }

    public function getId()
    {
        return $this->id;
    }

    public function thisEstablishment(){
        $this->andWhere(['establishment_id' => Yii::$app->user->identity->establishment_id]);
        return $this;
    }
}