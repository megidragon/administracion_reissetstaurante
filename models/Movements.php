<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Movements extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'movements';
    }

    public function getId()
    {
        return $this->id;
    }
}