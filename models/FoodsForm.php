<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class FoodsForm extends Model
{
    public $name;
    public $description;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [];
    }

    public static function customFilter($array)
    {
        if (empty($array)) return false;

        foreach ($array as $i => $element)
        {
            if (empty($element['name']) || (!empty($element['costo']) && !is_numeric($element['costo']))) unset($array[$i]);
        }

        return $array;
    }

}
