<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Orders extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'orders';
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTables(){
        return $this->hasOne(Tables::className(), ['id' => 'table_id']);
    }

    public function getMovements(){
        return $this->hasOne(Movements::className(), ['id' => 'movement_id']);
    }

    public function getOrderType(){
        return $this->hasOne(OrderType::className(), ['id' => 'order_type_id']);
    }

    public function getOrderPlates(){
        return $this->hasMany(OrderPlates::className(), ['order_id' => 'id']);
    }

    public function getPlates(){
        return $this->getOrderPlates()->joinWith('plates');
    }

    public function getFoods(){
        return $this->getOrderPlates()->joinWith('foods');
    }
}