<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UsersForm extends Model
{
    public $name;
    public $privileges;
    public $password;
    public $password_repeat;
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'privileges', 'email'], 'required', 'message' => '{attribute} es requerido.'],
            [['email', 'email_prueba'], 'email'],
            ['password', 'required', 'message'=>"Este campo es requerido"],
            ['password', 'string', 'min' => 6, 'tooShort' => 'Se requiere un minimo de 6 caracteres'],
            ['password_repeat', 'required', 'message'=>"Este campo es requerido"],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Las contraseñas no coinciden"],

            [['usuario', 'permiso', 'email_respuesta', 'password', 'email_prueba'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
        ];
    }
}
