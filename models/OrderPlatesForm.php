<?php

namespace app\models;

use Yii;
use yii\base\Model;

class OrderPlatesForm extends Model
{
    public $plate_id;
    public $food_id;
    public $amount;
    public $description;
    public $extra_cost;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['plate_id', 'amount'], 'required', 'message' => 'Este campo es requerido.'],
            [['descripton'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
            [['amount', 'extra_cost'], 'double', 'max' => 1000000, 'tooBig' => 'El sistema no soporta numeros tan grandes de {attribute}.', 'message' => 'Numero debe ser valido']
        ];
    }

    public function customFilter($request)
    {
        $valids = [];
        foreach ($request as $inputs)
        {
            if ($this->validate($inputs))
            {
                $valids[] = $inputs;
            }
        }
        return $valids;
    }
}
