<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Foods extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'foods';
    }

    public function getId()
    {
        return $this->id;
    }

    public static function softDeleteAll($condition)
    {
        $query = Yii::$app->db->createCommand()
            ->update(self::tableName(), ['deleted' => true], $condition);

        return $query->execute();
    }
}