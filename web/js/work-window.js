$(document).ready(function() {
    updateCards();
    setInterval(updateCards, 5000);
});


function updateCards(){
    $.get(url, (data) => {
        let container = $('#CardsContainer');
        container.html('');
        let template = '<div class="col-md-4 col-lg-3">\n' +
            '            <div class="card">\n' +
            '                <div class="card-head">\n' +
            '                    <p>{table}</p>\n' +
            '                </div>\n' +
            '                <div class="card-body">\n' +
            '                    {content}\n'+
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n';

        data.forEach((order, index) => {
            var content = '';
            order.plates.forEach((plate, pindex) => {
                let description = (plate.description) ? '<strong>( '+plate.description+' )</strong>' : '';
                let food = plate.food ? '/ '+plate.food : '';
                content += '<p>-'+plate.amount+' '+plate.plate+' '+food+' '+description+'</p>';
            });

            let card = template.replace('{table}', order.table);
            card = card.replace('{content}', content);
            container.append(card);
        });
    });
}