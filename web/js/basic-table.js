$(document).ready(function () {
    table = $('#datatable').DataTable({
        "processing": true,
        "serverSide": true,
        "orderMulti": false,
        "dom": '<"top"i>rt<"bottom"lp><"clear">',
        "lengthChange": false,
        "pageLength": 20,         "order": [[ 0, "desc" ]],
        "ajax": {
            "url": url,
            "type": "post",
            "datatype": "json",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "DateMinFilter": $('#DateMin').val(),
                    "DateMaxFilter": $('#DateMax').val(),
                    "StatusFilter": $('#StatusFilter').val(),
                } );
            },
        },
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "<i class='fa fa-circle-o-notch fa-spin' style='font-size: 30px'></i>",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
    });

    $('select, input').on('change', function () {
        search(table);
    });
    let buscador = $('#buscador');
    buscador.keyup(throttle(function(){search(table)}));
    buscador.siblings('input[type=button]').click(throttle(function(){search(table)}));
});

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 500);
    };
}

function search(table){
    table.columns(0).search($('#buscador').val());
    table.columns(1).search($('#buscador').val());
    table.draw();
}

function processButton(id){
    swal({
            title: 'Procesar el pedido como: ',
            type: 'warning',
            closeOnConfirm: false,
            closeOnCancel: true,
            showCancelButton: true,
            allowOutsideClick: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Completar orden',
            confirmButtonClass: 'btn-danger',
        },
        function(isConfirm) {
            if (isConfirm)
            {
                $.get(processUrl+'&id='+id+'&completed='+1, () => {
                    $('#buscador').trigger('keyup');
                    swal("Orden completada!", "Precione 'OK' para continuar.", "success");
                });
            }
        });
}