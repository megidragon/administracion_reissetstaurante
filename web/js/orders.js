can_submit = true;
total = 0;
discount = 0;
$(document).ready(function () {
    // Inizialisa todó lo necesario:
    updateTotals();


    $('#addPlate').click(function(){
        $('#PlateModal').modal('show')
    });

    $('#PlateForm').on('beforeSubmit', function () {
        // Primero se guarda el plato añadido
        let plate = [
            $('#PlateSelect option:selected').text(),
            $('#FoodSelect option:selected').text(),
        ];
        plate = plate.concat($(this).serializeArray().map(x => x['value']).splice(1, 5));

        plate_list.push(plate);

        // Calcula los totales
        updateTotals();

        // Cierra modal
        $(this).closest('#PlateModal').modal('toggle');
        $(this).closest('#PlateForm').trigger("reset");
        $('#PlateImg').attr("src", '/img/default.png');
        $('.select-picker').selectpicker('refresh');

        return false;
    });

    $('body').on('click', '.remove-item', function () {
        let target = $(this).data('index');
        plate_list.splice(target, 1);
        refreshTable();
    });

    $('#PlateSelect').on('change', function () {
        additional = $('#FoodSelect');
        id = $(this).val();

        $.get(additionalsUrl+'&id='+id, function (res)
        {
            additional.html('<option value="">Ninguno</option>');

            res.foods.map(function (v, i) {
                additional.append('<option value="'+v.id+'">'+v.name+' + ( $'+v.cost+' )</option>');
            });

            $('#PlateCost').val(res.data.cost);
            $('.select-picker').selectpicker('refresh');

            $('#PlateImg').attr('src', res.data.img_url.replace('@web', ''))
        });


    });
    $('#OrderForm').on('beforeSubmit', function(e){
        if (!can_submit) {
            return false;
        }
        if (plate_list.length < 1)
        {
            swal({
                title: 'Se requiere al menos 1 plato para crear el pedido',
                type: 'error',
                showCancelButton: false,
                closeOnConfirm: true,
                allowOutsideClick: true
            });
            return false;
        }

        let form = $(this);

        startSubmit();
        $.post(form.attr("action"), form.serialize())
            .done(function(result){
                if (result.error){
                    swal({
                        title: result.msg,
                        type: 'error',
                        showCancelButton: false,
                        closeOnConfirm: true,
                        allowOutsideClick: true
                    });

                    return;
                }

                if (result.redirect)
                {
                    swal({
                        title: result.msg,
                        type: 'success',
                        showCancelButton: false,
                        closeOnConfirm: true,
                        allowOutsideClick: true
                    });
                    setTimeout(() => { location.href = result.redirect; }, 1500);
                }

                /*alert("se encontro un error no definido.");*/

            }).fail(function (error) {
            endSubmit();
            return false;
        });

        return false;
    });
});

function refreshTable(){
    let list = $('#PlateList tbody');
    let plate_list_hidden = $('#PlateListHidden');
    list.html("");
    plate_list_hidden.html("");
    $('#Total').text(total);
    $('#Discount').text(discount);

    plate_list.forEach(function(value, index)
    {
        list.append(
            "<tr>" +
            "<td>"+value[0]+"</td>"+
            "<td>"+value[1]+"</td>"+
            "<td>"+roundToTwo(value[4])+"</td>"+
            "<td>"+(value[5] === "" ? '-' : value[5])+"</td>"+
            "<td>$"+(value[6] === "" ? '0' : value[6])+"</td>"+
            "<td>$"+roundToTwo(value[7])+"</td>"+
            "<td><button type=\"button\" title=\"Remover complemento\" class=\"remove-item btn btn-danger btn-xs\" data-index='"+index+"''><i class=\"glyphicon glyphicon-minus\"></i></button></td>"+
            "</tr>"
        );

        plate_list_hidden.append('<input type="hidden" name="plates['+index+'][plate_id]" value="'+value[2]+'">');
        plate_list_hidden.append('<input type="hidden" name="plates['+index+'][food_id]" value="'+value[3]+'">');
        plate_list_hidden.append('<input type="hidden" name="plates['+index+'][amount]" value="'+value[4]+'">');
        plate_list_hidden.append('<input type="hidden" name="plates['+index+'][description]" value="'+value[5]+'">');
        plate_list_hidden.append('<input type="hidden" name="plates['+index+'][extra_cost]" value="'+value[6]+'">');
    });
}

function updateTotals(){
    if (plate_list.length > 0) {
        $.post(totalsUrl, {'data': plate_list, 'form_data': $('#OrderForm').serializeArray()}, function (res) {
            plate_list = res.plates;
            discount = res.discount;
            total = res.total;
            // Trae el array guardado y lo lista tódo nuevamente.
            refreshTable();
        });
    }
}


function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

function startSubmit(){
    can_submit = false;
    if ($('.loader').is(":visible")) {
        return;
    }
    toogleLoader();
}

function endSubmit(){
    can_submit = true;
    $('.loader').fadeOut(200);
}