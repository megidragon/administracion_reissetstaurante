<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="x_content">
<div class="row">
    <h1>Pantalla de pedidos</h1>
</div>

<div class="row" id="CardsContainer">

</div>

</div>
<script>
    url = '<?=Url::to(['work-window/data']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/work-window.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>