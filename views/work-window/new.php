<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Crear nueva plato</h2>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'dynamic-form']]) ?>

<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del plato') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'description')->textInput()->input('text', ['placeholder' => 'Descripcion'])->label('Corta descripcion del plato') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'cost')->textInput()->input('number', ['placeholder' => 'Valor', 'step' => 'any'])->label('Valor del plato') ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'img_url')->fileInput() ?>
        </div>
    </div>
</div>


<?= $this->render('componentsElement', ['form' => $form, 'foodsModel' => $foodsModel]) ?>

<div class="form-group">
    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
</div>


<?php ActiveForm::end() ?>