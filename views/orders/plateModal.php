<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use brussens\bootstrap\select\Widget as Select;
use yii\helpers\ArrayHelper;

Modal::begin([
    'header' => '<h4>Plato</h4>',
    'id'     => 'PlateModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'PlateForm']]) ?>
        <div class="row">
            <div class="col-md-12">
            <?=$form->field($model, "plate_id")->widget(Select::className(), [
                'options' => [
                    'data-live-search' => 'true',
                    'prompt'=>'Seleccione',
                    'class' => 'form-control plate',
                    'id' => 'PlateSelect'
                ],
                'items' => ArrayHelper::map($plateList, 'id', 'name')
            ])->label('Plato');?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            <?=$form->field($model, "food_id")->widget(Select::className(), [
                'options' => ['data-live-search' => 'true', 'prompt'=>'Ninguno', 'class' => 'form-control', 'id' => "FoodSelect"],
                'items' => []
            ])->label('Acompañamiento')?>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, "amount")->input('number', ['maxlength' => true, 'placeholder' => 'Cantidad'])->label('Cantidad') ?>
            </div>
            <div class="col-md-2">
                <?=Html::label('Costo (U)'); ?>
                <?= Html::input('number', '', 0, ['type' => "number", 'class' => 'form-control', "id" => "PlateCost", "disabled" => "disabled"]); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, "description")->textInput(['placeholder' => 'Especificaciones', 'maxlength' => true])->label('Especificaciones') ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, "extra_cost")->input('number', ['maxlength' => true, 'placeholder' => 'Valor agregado', 'step' => 'any'])->label('Costo ad. (U)') ?>
            </div>
        </div>


        <div class="row image-container">
            <?= Html::img('@web/img/default.png', ['class' => 'image-preview', 'id' => 'PlateImg']) ?>
        </div>

        <div class="row">
            <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary btn-xl']); ?>
        </div>
        <?php $form->end() ?>
    </div>
<?php Modal::end(); ?>