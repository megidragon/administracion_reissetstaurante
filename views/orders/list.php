<?php
use  yii\helpers\Html;
use  yii\helpers\Url;
use brussens\bootstrap\select\Widget as Select;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
?>


<div class="x_content">
    <div class="row">
        <h1>Menu 1</h1>
    </div>
    <div class="row">
        <div class="col-md-4 input-group">
            <input type="text" placeholder="Buscador" id="buscador" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
        </div>
        <div class="col-md-2">
            <?= Html::a('<button class="btn btn-primary"><i class="fa fa-plus"></i> Crear nueva</button>', ['orders/new']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= Html::label('Estado', 'StatusFilter') ?>
            <?= Select::widget([
                'name' => 'statusFilter',
                'value' => '0',
                'items' => ['1' => 'Completado', '0' => 'En preparacion', '-1' => 'Cancelado'],
                'options' => [
                    'id' => 'StatusFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <div class="col-md-6">
                <?= Html::label('Desde', 'DateMin') ?>
                <?= DatePicker::widget([
                    'language' => 'es',
                    'dateFormat' => 'dd / MM / yyyy',
                    'value' => date('d / m / Y'),
                    'options' => ['id' => 'DateMin', 'class' => 'form-control', 'autocomplete' => "off"]
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= Html::label('Hasta', 'DateMax') ?>
                <?= DatePicker::widget([
                    'language' => 'es',
                    'dateFormat' => 'dd / MM / yyyy',
                    'value' => date('d / m / Y', strtotime("+1 day", strtotime(date('Y-m-d')))),
                    'options' => ['id' => 'DateMax', 'class' => 'form-control', 'autocomplete' => "off"]
                ]) ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="7%">Mesa</th>
                <th>Fecha pedido</th>
                <th>Costo</th>
                <th width="50%">Descripcion</th>
                <th>Estado</th>
                <th width="12%">accion</th>
            </tr>
            </thead>
        </table>
    </div>

</div>

<script>
    url = '<?=Url::to(['orders/data']);?>';
    processUrl = '<?=Url::to(['orders/process-order']);?>';
</script>
<?php
$this->registerJsFile(
    '@web/js/basic-table.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>