<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="form-group">
        <div class="container-items"><!-- widgetContainer -->
            <div class="panel panel-default"><!-- widgetBody -->
                <div class="panel-heading">
                    <h3 class="panel-title pull-left col-sm-1">Platos</h3>
                    <div class="pull-left col-sm-1">
                        <?= Html::button('<i class="glyphicon glyphicon-plus"></i>Agregar plato', ['id' => 'addPlate', 'class' => 'btn btn-success']) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body item-container">
                    <div  style="overflow-x:auto;">
                        <table class="table table-striped table-responsive" id="PlateList">
                            <thead>
                            <tr>
                                <th>Plato</th>
                                <th>Acompañamiento</th>
                                <th>Cantidad</th>
                                <th>Especificacion</th>
                                <th>Costo adicional</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="7">Vacia</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="PlateListHidden"></div>
                </div>
                <div class="panel-footer">
                    <table class="table table-striped table-responsive">
                        <tr>
                            <td width="70%"></td>
                            <td>Descuento</td>
                            <td>$<span id="Discount">0</span></td>
                        </tr>
                        <tr>
                            <td width="70%"></td>
                            <td>Total</td>
                            <td>$<span id="Total">0</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>