<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use brussens\bootstrap\select\Widget as Select;
?>

<h2>Modificar orden</h2>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'OrderForm']]) ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-3">
                <?=$form->field($model, 'order_type_id')->widget(Select::className(), [
                    'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                    'items' => ArrayHelper::map($order_types, 'id', 'name')
                ])->label('Tipo de pedido');?>
            </div>
            <div class="col-md-2">
                <?=$form->field($model, 'table_id')->widget(Select::className(), [
                    'options' => ['data-live-search' => 'true'],
                    'items' => ArrayHelper::map($tables, 'id', 'name')
                ])->label('Mesa del pedido');?>
            </div>
            <div class="col-md-2">
                <?=$form->field($model, 'offer_id')->widget(Select::className(), [
                    'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione', 'disabled' => true],
                    'items' => ArrayHelper::map($offers, 'id', 'name')
                ])->label('Oferta a aplicar');?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'coupon')->input('text', ['placeholder' => 'Ej: Px3H1314I', 'disabled' => true])->label('Cupon de descuento') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'additional')->input('number', ['placeholder' => 'Valor agregado', 'step' => 'any'])->label('Valor adicional') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-5">
                <?= $form->field($model, 'observations')->textarea(['placeholder' => 'Detalles del pedido'])->label('Detalles del pedido') ?>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="form-group">
            <div class="container-items"><!-- widgetContainer -->
                <div class="panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left col-sm-1">Platos</h3>
                        <div class="pull-left col-sm-1">
                            <?= Html::button('<i class="glyphicon glyphicon-plus"></i>Agregar plato', ['id' => 'addPlate', 'class' => 'btn btn-success']) ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body item-container">
                        <?=$this->render('platesElement.php', ['plateList' => $plateList])?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary btn-lg']); ?>
    </div>
<?php ActiveForm::end() ?>


<?=$this->render('plateModal.php', ['model' => $platesModel, 'plateList' => $plateList, 'form' => $form])?>


<script>
    additionalsUrl = '<?=Url::to(['plates/get-plate'])?>';
    totalsUrl = '<?=Url::to(['orders/calculate-totals'])?>';
    plate_list = <?=Json::encode($orderPlates);?>;
</script>


<?php
$this->registerJsFile(
    '@web/js/orders.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>