<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>


<h2>Modificar plato <?=$model->name?></h2>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'dynamic-form']]) ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del plato') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'description')->textInput()->input('text', ['placeholder' => 'Descripcion'])->label('Corta descripcion del plato') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'cost')->textInput()->input('number', ['placeholder' => 'Valor', 'step' => 'any'])->label('Valor del plato') ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'img_url')->fileInput() ?>
                <label>Imagen actual</label>
                <br>
                <?=Html::img($model->img_url, ['class' => 'pull-left img-responsive image-preview']); ?>
            </div>
        </div>
    </div>
    <br>
<?= $this->render('componentsElement', ['form' => $form, 'foodsModel' => $foodsModel]) ?>

    <br>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
    </div>

<?php $form->end() ?>