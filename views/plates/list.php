<?php
use  yii\helpers\Html;
use  yii\helpers\Url;
?>


<div class="x_content">
    <div class="row">
        <h1>Menu 1</h1>
    </div>
    <div class="row">
        <div class="col-md-4 input-group">
            <input type="text" placeholder="Buscador" id="buscador" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
        </div>
        <div class="col-md-2">
            <?= Html::a('<button class="btn btn-primary"><i class="fa fa-plus"></i> Crear nueva</button>', ['plates/new']) ?>
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="7%">Foto</th>
                <th width="8%">Fecha Creacion</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Costo</th>
                <th width="8%">accion</th>
            </tr>
            </thead>
        </table>
    </div>

</div>
<script>
    url = '<?=Url::to(['plates/data']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/basic-table.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>