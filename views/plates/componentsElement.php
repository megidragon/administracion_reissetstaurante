<div class="row">
    <div class="form-group">
        <?php use wbraganca\dynamicform\DynamicFormWidget;
        use yii\helpers\Html;

        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.panel-body', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $foodsModel[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'name',
                'description',
                'cost',
            ],
        ]); ?>
        <div class="container-items"><!-- widgetContainer -->
            <div class="panel panel-default"><!-- widgetBody -->
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Complementos</h3>
                    <div class="pull-right">
                        <button type="button" title="Añadir nuevo" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php foreach ($foodsModel as $i => $food){ ?>
                        <?php
                        // necessary for update action.
                        if (! $food->isNewRecord) {
                            echo Html::activeHiddenInput($food, "[{$i}]id");
                        }
                        ?>
                        <div class="item row">
                            <div class="col-sm-3">
                                <?= $form->field($food, "[{$i}]name")->textInput(['placeholder' => 'Nombre de la comida', 'maxlength' => true])->label('Nombre') ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->field($food, "[{$i}]description")->textInput(['placeholder' => 'Descripcion', 'maxlength' => true])->label('Descripcion') ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->field($food, "[{$i}]cost")->input('number', ['maxlength' => true, 'placeholder' => 'Valor agregado', 'step' => 'any'])->label('Costo adicional') ?>
                            </div>
                            <div class="col-md-3"><br>
                                <button type="button" title="Remover complemento" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                        </div><!-- .row -->
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
</div>