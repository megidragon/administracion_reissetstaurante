<?php
use  yii\helpers\Html;
use  yii\helpers\Url;
use brussens\bootstrap\select\Widget as Select;

?>


<div class="x_content">
    <div class="row">
        <h1>Listado de usuarios</h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="input-group">
                <?= Html::input('text', '', '', ['class' => 'form-control', 'id' => 'buscador', 'placeholder' => 'Buscador']) ?>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Buscar</button>
                </span>
            </div>
        </div>
        <div class="col-md-2">
            <?= Html::a('<button class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</button>', ['usuario/nuevo']) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <?= Html::label('Rol de usuario', 'RoleFilter') ?>
            <?= Select::widget([
                'name' => 'roleFilter',
                'items' => ['administrador' => 'Administrador', 'sincronizador' => 'Sincronizador', 'autorizador' => 'Autorizador', 'basico' => 'Basico'],
                'options' => [
                    'id' => 'RoleFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::label('Estado', 'StatusFilter') ?>
            <?= Select::widget([
                'name' => 'statusFilter',
                'items' => ['1' => 'Activo', '0' => 'Inabilitado'],
                'options' => [
                    'id' => 'StatusFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="12%">Ultima actualizacion</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Email</th>
                <th>Estado</th>
                <th width="8%">Accion</th>
            </tr>
            </thead>
        </table>
    </div>

</div>
<script>
    url = '<?=Url::to(['usuario/data']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/usuario.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>