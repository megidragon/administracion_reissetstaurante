<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <div class="col-md-3 col-md-offset-5">
        <i class="fa fa-exclamation-triangle" style="font-size: 275px; color: red;"></i><br>
        <h3>Se encontro el siguiente error: </h3>
        <h4><?= nl2br(Html::encode($message)) ?></h4>
    </div>
</div>
