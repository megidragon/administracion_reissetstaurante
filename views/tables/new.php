<?php

use yii\helpers\ArrayHelper;
use brussens\bootstrap\select\Widget as Select;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Crear nueva mesa</h2>
<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del plato') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?=$form->field($model, 'establishment_id')->widget(Select::className(), [
                'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                'items' => ArrayHelper::map($establishments, 'id', 'name')
            ])->label('Establecimiento');?>
        </div>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
</div>


<?php ActiveForm::end() ?>